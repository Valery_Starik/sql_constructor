﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows;
using ApplicationDB;
using ApplicationDB.Common;
using ApplicationDB.Exception;
using ServicesLib;


namespace Manager
{
    public class Database
    {

        public static EntityManager manager = EntityManager.GetInstance();
        public static AUser user;

        public static bool TestConnectDB(string StringConnect)
        {
            /// <summary>
            /// Метод тестирующий подключение к БД. 
            /// </summary>

            try
            {
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = StringConnect;
                connection.Open();
                if (connection.State.ToString() == "Open")
                {
                    connection.Close();
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception)
            {
                return false;
            }
        }

        public static void SendDataToBD(string[] conn)
        {
            //This Data of save to DB application
        }

        /// <summary>
        /// Gets the connection string for transmission Workspace.xml
        /// </summary>

        public static bool LogInUser(string Name, string Pass)
        {
            try
            {
                user = manager.LoginUser(Name, Pass);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool RegisterUser(string Name, string Pass)

        {
            try
            {
                manager.RegistrationUser(Name, Pass);
                string text =
                    String.Format("{0}, thank's for registration in SQL Constructor\n Your login: {0}\n Your password: {1}",
                    Name, Pass);
                user = manager.LoginUser(Name, Pass);
                SmtpMailer mail = SmtpMailer.Instance();
                mail.SendMail(Name, "Registration in SQL Constructor", text);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static DbSchema CreateSchemaInstance(string connString)
        {
            return new DbSchema(connString);
        }

    }


}
