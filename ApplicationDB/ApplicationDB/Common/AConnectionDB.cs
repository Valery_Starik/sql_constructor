﻿namespace ApplicationDB.Common
{
    public class AConnectionDB
    {
        public int ID { get; set; }
        public string ConnectionName { get; set; }
        public string ServerName { get; set; }
        public string DatabaseName { get; set; }
        public string LoginDB { get; set; }
        public string PasswordDB { get; set; }

    }
}