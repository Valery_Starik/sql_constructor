﻿namespace ApplicationDB.Common
{
    public abstract class AProject
    {
        public int ID { get; set; }
        public string ProjectName { get; set; }
        public AConnectionDB Connection { get; set; }
    }
}