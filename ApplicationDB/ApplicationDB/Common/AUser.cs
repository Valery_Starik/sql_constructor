﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationDB.Common
{
   public abstract class AUser
    {
        public string Email { get; set; }
        public IList<AProject> MyProjects { get; set; }
        public IList<AProject> ShareProjects { get; set; }
    }
}
