﻿using ApplicationDB.Common;
using ApplicationDB.Entity;
using ApplicationDB.Exception;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Security;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationDB
{
   public class EntityManager
    {
        private DbConnection _connection  = new SqlConnection();
        private IDbDataAdapter _adapter = new SqlDataAdapter();

        private static EntityManager _instance;

        public DbConnection Connection
        {
            set
            {
                _connection = value;
            }
        }

        public IDbDataAdapter Adapter
        {
            
            set
            {
                _adapter = value;
            }
        }

        private EntityManager() { }
        
        public static EntityManager GetInstance()
        {
            if (_instance == null)
            {
                _instance = new EntityManager();
            }
            return _instance;
        }


        /// <summary>
        /// Метод добавляет запись о новом пользователе в базу данных. 
        /// Если пользователь с таким email уже существует - throw new MailIsBusyException();
        /// </summary>
        /// <param name="email"></param>
        /// <param name="password"></param>
       
        public void RegistrationUser(string email, string password)
        {
            using (DbConnection conn = _connection)
            {
                conn.ConnectionString = Resource.ConnectionString;
                IDbCommand cmd = conn.CreateCommand();
                cmd.CommandText = string.Format("USE SqlConstructorDB SELECT Email FROM Users WHERE Email ='{0}'", email);

                IDbDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = cmd;
                conn.Open();

                DataSet ds = new DataSet();
                int count = adapter.Fill(ds);
                
                if (count != 0)
                {
                    throw new MailIsBusyException();
                }

                cmd.CommandText = string.Format("USE SqlConstructorDB INSERT INTO Users(Email, PasswordHash) VALUES('{0}', '{1}');",email, GetHashString(password));
                adapter.UpdateCommand = cmd;
                adapter.Fill(ds);
            }
            
        }
       


        /// <summary>
        ///Находит пользователя в БД с данным email, проверяет правильность пароля,
        ///возвращает пользователя со всеми его данными.
        ///В случае несовпадения пароля или почты - throw new ArgumentException();
        /// </summary>
        /// <param name="email"></param>
        /// <param name="password"></param>
        /// <returns></returns>

        public AUser LoginUser(string email, string password)
        {
            if(ValidationUser(email, password))
            {
                IList<AProject> MyProjects = LoadMyProject(email);
                IList<AProject> ShareProjects = LoadShareProject(email);
                AUser user = new User{Email = email, MyProjects = MyProjects, ShareProjects = ShareProjects};
                return user;
            }
            throw new ArgumentException();
        }

        /// <summary>
        /// Если проект новый - добавляет новую запись в БД
        /// Если же идет сохранение ранее созданого проекта, то обновляет запись в БД.
        /// </summary>
        /// <param name="project"></param>
        public void SaveProject(AProject project)
        {
          
        }
        /// <summary>
        /// Можно будет использовать или для кнопки обновить 
        /// Или для обработчика какого-то события...
        /// 
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public IList<AProject> LoadMyProject(string email)
        {
            return GetProject(string.Format("USE SqlConstructorDB SELECT ProjectID, ProjectName FROM Projects WHERE ProjectOwner ='{0}'", email));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public IList<AProject> LoadShareProject(string email)
        {
            return GetProject(string.Format("USE SqlConstructorDB SELECT p.ProjectID, p.ProjectName FROM ProjectsShare AS ps, Projects AS p WHERE ps.ProjectID = p.ProjectID AND SharedEmail ='{0}'", email));
        }

        

        private IList<AProject> GetProject(string command)
        {
            IList<AProject> result = new List<AProject>();

            using (DbConnection conn = _connection)
            {
                conn.ConnectionString = Resource.ConnectionString;
                IDbCommand cmd = conn.CreateCommand();
                cmd.CommandText = command;
                conn.Open();

                IDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    AProject temp = new Project();
                    temp.ID = reader.GetInt32(0);
                    temp.ProjectName = reader.GetString(1);
                    temp.Connection = GetConnectionByProjectID(temp.ID);

                    result.Add(temp);
                }
                reader.Close();
            }
            return result;
        }


        private AConnectionDB GetConnectionByProjectID(int iD)
        {
            AConnectionDB result = new ConnectionDB();
            using (DbConnection conn = _connection)
            {
                conn.ConnectionString = Resource.ConnectionString;
                IDbCommand cmd = conn.CreateCommand();
                cmd.CommandText = string.Format(@"USE SqlConstructorDB
                            SELECT ConnectionID, ConnectionName, ServerName, DatabaseName, LoginDB, PasswordDB
                            FROM ConnectionDB
                            WHERE ConnectionOwner = {0}", iD);
                conn.Open();

                IDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                   AConnectionDB temp = new ConnectionDB();

                    temp.ID = reader.GetInt32(0);
                    temp.ConnectionName = reader.GetString(1);
                    temp.ServerName = reader.GetString(2);
                    temp.DatabaseName = reader.GetString(3);
                    temp.LoginDB = reader.GetString(4);
                    temp.PasswordDB = reader.GetString(5);
                    result = temp;
                }
                reader.Close();
            }
            return result;
        }

       




        private bool ValidationUser(string email, string password)
        {
            bool result = false;

            using (DbConnection conn = _connection)
            {
                conn.ConnectionString = Resource.ConnectionString;
                IDbCommand cmd = conn.CreateCommand();
                cmd.CommandText = string.Format("USE SqlConstructorDB SELECT Email, PasswordHash FROM Users WHERE Email ='{0}'", email);
                conn.Open();
                
                IDataReader reader = cmd.ExecuteReader();
                
                while (reader.Read())
                {
                     string tempMail = reader.GetString(0);
                     string tempPass = reader.GetGuid(1).ToString();
                     result = (email.Equals(tempMail) && GetHashString(password).ToString().Equals(tempPass));
                }
                reader.Close();
            
            }
            return result;
        }
        private Guid GetHashString(string pass)
        {

            byte[] bytes = Encoding.Unicode.GetBytes(pass);
            MD5CryptoServiceProvider CSP = new MD5CryptoServiceProvider();

            byte[] byteHash = CSP.ComputeHash(bytes);

            string hash = string.Empty;

            foreach (byte b in byteHash)
                hash += string.Format("{0:x2}", b);

            return new Guid(hash);
        }
    }
}
