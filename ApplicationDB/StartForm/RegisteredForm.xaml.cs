﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Manager;

namespace StartForm
{
    /// <summary>
    /// Interaction logic for RegisteredForm.xaml
    /// </summary>
    public partial class RegisteredForm : Window
    {
        public RegisteredForm()
        {
            InitializeComponent();
        }

       
        private void button_Copy_Click(object sender, RoutedEventArgs e)
        {
            MainWindow Mw = new MainWindow();
            Mw.Show();
            this.Close();
        }

       private void button_Click(object sender, RoutedEventArgs e)
        {
            if ((MainWindow.EmailValidation(textBoxMail.Text)) && (MainWindow.PassValidation(passwordBox.Password)))
            {
                
                    if (Database.RegisterUser(textBoxMail.Text.ToLower(), passwordBox.Password))
                    {
                        Workspace ws = new Workspace();
                        ws.Show();
                        this.Close();
                    }
                    else
                    {
                    MessageBox.Show("Database has user with your e-mail");
                    MainWindow mw = new MainWindow();
                    mw.Show();
                        mw.TextBoxUser.Text = textBoxMail.Text;
                        this.Close();
                    }
            
            }
            else
            {
                MessageBox.Show("Enter correct E-mail/password");
            }
        }
    }
}
