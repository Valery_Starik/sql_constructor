﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Binding = System.Windows.Data.Binding;
using TreeView = System.Windows.Controls.TreeView;
using Manager;
using ApplicationDB;
using System.Data.SqlClient;
using System.Data;
using System.IO;

namespace StartForm
{
    public partial class Workspace : Window
    {
        public Workspace()
        {
            InitializeComponent();
           // UpdateTableTree("Data Source=(localdb)\\ProjectsV12;Initial Catalog=Northwind;Integrated Security=True");
        }

        //public void Shema(string str)
        //{
        //    using (SqlConnection conn = new SqlConnection(str))
        //    {
        //        DbSchema ent = new DbSchema(str);
        //        conn.Open();
        //        DataTable allColumnsSchemaTable = conn.GetSchema("Columns");
        //        //TreeViewSchema.DataContext = ent.GetTableEntities(ent);
        //        conn.Close();
        //    }
        //}

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            ConnectDB winConnectDb = new ConnectDB();
            winConnectDb.Show();
        }

        private void MenuItem_Click_Exit(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void MenuItem_Click_LogOut(object sender, RoutedEventArgs e)
        {
            MainWindow mw = new MainWindow();
            mw.ShowDialog();
            this.Close();
        }


        //private void Change_DataSource(object sender, RoutedEventArgs e)
        //{

        //        listBooks.ItemTemplate = null;
        //    treeStructure.ItemsSource = ConnectDB.ConnectWithDB(ConnectDB.GetConnectionString()).Namespace;
        //        treeStructure.ItemTemplate = (HierarchicalDataTemplate)FindResource("key1");
        //        //Настраиваем привязку примечаний
        //        DescriptionBinding(selectedNodeDescription, "SelectedItem.Description", treeStructure);
        //        //DescriptionBinding(selectedBookDescription, "SelectedItem.Description", listBooks);
        //        //Настраиваем привязку отображения книг
        //        Binding bind = new Binding("SelectedItem.Books") { Source = treeStructure };
        //        listBooks.SetBinding(ItemsControl.ItemsSourceProperty, bind);


        //}

        ///// <summary>
        ///// Настройка привязки отображения данных
        ///// </summary>
        ///// <param name="textBlock">Текстовый объект, который должен отображать текст примечания</param>
        ///// <param name="pathValue">значение Path привязки</param>
        ///// <param name="source">ссылка на объект-источник, из которого считываются данные через свойство Path</param>
        //void DescriptionBinding(TextBlock textBlock, string pathValue, TreeView source)
        //{
        //    textBlock.SetBinding(TextBlock.TextProperty, new Binding(pathValue) { Source = source });
        //}

        #region Fill Tree view
        public void UpdateTableTree(string connection)
        {
            TreeViewSchema.Items.Clear();
                DbSchema shema = new DbSchema(connection);
                TreeViewItem tables = new TreeViewItem();
                tables.Tag = "Tables";
                tables.Header = "Tables";
                TreeViewSchema.Items.Add(tables);
            TreeViewItem views = new TreeViewItem();
                views.Tag = "Views";
                views.Header = "Views";
                TreeViewSchema.Items.Add(views);

                if (shema != null)
                {
                    foreach (DataTable dt in shema.Tables)
                    {
                        var node = new TreeViewItem();
                        node.Header = dt.ToString();
                        switch (DbSchema.GetTableType(dt))
                        {
                            case TableType.Table:
                                tables.Items.Add(node);
                                AddDataColumns(node, dt);
                                break;
                            case TableType.View:
                                views.Items.Add(node);
                                AddDataColumns(node, dt);
                                break;
                        }
                    }
            }
        }

        void AddDataColumns(TreeViewItem node, DataTable dt)
        {
            foreach (DataColumn col in dt.Columns)
            {
                node.Items.Add(col.ColumnName);
            }
        }
        #endregion
    }
}
