﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Data.SqlClient;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Manager;
using System.Data;
using ApplicationDB;

namespace StartForm
{
    /// <summary>
    /// Interaction logic for ConnectDB.xaml
    /// </summary>
    
    public partial class ConnectDB : Window
    {
        public ConnectDB()
        {
            InitializeComponent();
        }

        private void TestConnect_Click(object sender, RoutedEventArgs e)
        {
            /// <summary>
            /// Метод выводит результат тестирования соединения с БД. 
            /// </summary>

            if (WorkWithDatabase.TestConnectDB(StringConnect()))
           MessageBox.Show("True");
            else
           MessageBox.Show("False");
        }

        public string StringConnect()
        {
            /// <summary>
            /// Метод генерирует строку подключения к БД. 
            /// </summary>

            if (WindowsAutorizedCheck.IsChecked == false)
            {
                 return String.Format("Server = {0}; Database = {1}; User ID = {2}; Password = {3};",
                    ServerBox.Text, DatabaseBox.Text, UserIdBox.Text, passwordBox.Password);
            }
            else
            {
               return  String.Format("Server = {0}; Database = {1}; Integrated security = {2}",
                    ServerBox.Text, DatabaseBox.Text, "SSPI");
            }
        }

        public string[] ConnectDb()
        {
            /// <summary>
            /// Метод возвращает массив параметров подключения к БД. 
            /// </summary>
            string[] conn = new[] { ServerBox.Text, DatabaseBox.Text, UserIdBox.Text, passwordBox.Password };
            return conn;
        }

        private void ConnectDBButton_OnClick(object sender, RoutedEventArgs e)
        {
            /// <summary>
            /// Метод обработки чекбокса для авторизации с помощью аккаунта Виндовс. 
            /// </summary>

            if (WindowsAutorizedCheck.IsChecked == true)
            {
                UserIdBox.IsEnabled = false;
                passwordBox.IsEnabled = false;
            }
            if (WindowsAutorizedCheck.IsChecked == false)
            {
                UserIdBox.IsEnabled = true;
                passwordBox.IsEnabled = true;
            }
        }

        private void AddConnectButton_OnClick(object sender, RoutedEventArgs e)
        {
            /// <summary>
            /// Метод передает параметры подключения в менеджер для дальнейшего использования. 
            /// </summary>
            
            Database.SendDataToBD(ConnectDb());
            Workspace ws = new Workspace();
            ws.UpdateTableTree(StringConnect());
            

            this.Close();
        }
        
        
    }
}
