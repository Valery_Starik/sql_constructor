﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Text.RegularExpressions;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ApplicationDB;
using ApplicationDB.Common;
using Manager;

namespace StartForm
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            App.LanguageChanged += LanguageChanged;

            CultureInfo currLang = App.Language;
                //Заполняем меню смены языка:
            menuLanguage.Items.Clear();
            foreach (var lang in App.Languages)
            {
                MenuItem menuLang = new MenuItem();
                menuLang.Header = lang.DisplayName;
                menuLang.Tag = lang;
                menuLang.IsChecked = lang.Equals(currLang);
                menuLang.Click += ChangeLanguageClick;
                menuLanguage.Items.Add(menuLang);
            }

        }

        private void ChangeLanguageClick(Object sender, EventArgs e)
        {
            MenuItem mi = sender as MenuItem;
            if (mi != null)
            {
                CultureInfo lang = mi.Tag as CultureInfo;
                if (lang != null)
                {
                    App.Language = lang;
                }
            }

        }


        private void LanguageChanged(Object sender, EventArgs e)
        {
            CultureInfo currLang = App.Language;

            //Отмечаем нужный пункт смены языка как выбранный язык
            foreach (MenuItem i in menuLanguage.Items)
            {
                CultureInfo ci = i.Tag as CultureInfo;
                i.IsChecked = ci != null && ci.Equals(currLang);
            }
        }


        private void checkBox_Click(object sender, RoutedEventArgs e)
        {
            if (checkBoxNotReg.IsChecked == true)
            {
                TextBoxUser.IsEnabled = false;
                TextBoxUser.Text = "NotRegister";
                passwordBox.IsEnabled = false;
            }
            if (checkBoxNotReg.IsChecked == false)
            {
                TextBoxUser.IsEnabled = true;
                TextBoxUser.Text = null;
                passwordBox.IsEnabled = true;
            }
            
        }

        private void ButtonNewUser_Click(object sender, RoutedEventArgs e)
        {
            /// <summary>
            /// Обработчик нажатия на кнопку "Новый пользователь" Вызывает форму регистрации. 
            /// </summary>

            RegisteredForm Rf = new RegisteredForm();
            Rf.Show();
            this.Close();
        }

        private void ButtonLogIn_Click(object sender, RoutedEventArgs e)
        {
            /// <summary>
            /// Обработчик нажатия на кнопку "Залогиниться".
            /// Проводит валидацию по мылу и паролю, запускает Главную рабочую форму 
            /// </summary>

            if ((EmailValidation(TextBoxUser.Text)) && (PassValidation(passwordBox.Password)))
            {
                if (Database.LogInUser(TextBoxUser.Text.ToLower(), passwordBox.Password))
                {
                    Workspace ws = new Workspace();
                    ws.Show();
                    this.Close();
                }
                else
                {
                    MessageBox.Show("This user is not register");
                }
                
            }
            if((bool)checkBoxNotReg.IsChecked)
            {
                Workspace ws = new Workspace();
                ws.Show();
                
                this.Close();
            }
            else
            {
                MessageBox.Show("Enter correct E-mail/password");
            }

        }

        public static bool EmailValidation(string txt)
        {
            /// <summary>
            /// Метод валидации мыла. 
            /// </summary>
            
            string email = @"^([A-Za-z0-9_-]+\.)*[A-Za-z0-9_-]+@[A-Za-z0-9_-]+(\.[A-Za-z0-9_-]+)*\.[A-Za-z]{2,6}$";
            if (Regex.IsMatch(txt, email))
            {
                return true;
            }
            else return false;
        }

        public static bool PassValidation(string txt)
        {
            /// <summary>
            /// Метод валидации пароля. 
            /// </summary>

            string pass = @"^(?=.*\d)(?=.*[a-zA-Z0-9])(?=.*[A-Za-z0-9])(?!.*\s).*$";
            if (Regex.IsMatch(txt, pass))
            {
                return true;
            }
            else return false;
        }


    }
}
