﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Data.ConnectionUI;


namespace DatabaseConnect
{
    public class ConnectDB
    {
        public static string GetConnectionString()
        {
            DataConnectionDialog dataConnection = new DataConnectionDialog();
            DataSource.AddStandardDataSources(dataConnection);
            
            string connectionString = null;
            if (DataConnectionDialog.Show(dataConnection) == DialogResult.OK)
            {
                connectionString = dataConnection.ConnectionString;
            }
            return connectionString;
        }

        public static void ConnectWithDB(string connectionString)
        {
            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();
            //DataTable tables = conn.GetSchema("Tables");
            //DataTable columns = conn.GetSchema("Columns");
            conn.Close();
            }

        

       
    }


}
