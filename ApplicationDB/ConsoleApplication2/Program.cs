﻿using ApplicationDB;
using ApplicationDB.Common;
using ApplicationDB.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication2
{
    class Program
    {
        static void Main(string[] args)
        {
            EntityManager manager = EntityManager.GetInstance();

          
            AUser user = manager.LoginUser("v.isaenko@uk.net", "123456");
            Console.WriteLine(user.Email);
            Console.WriteLine(user.MyProjects.Count);
            Console.Read();
        }
    }
}
